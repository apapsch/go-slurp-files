package slurpfiles

import (
	"context"
	"fmt"
	"io"

	"codeberg.org/apapsch/go-aggregate-error"
	"github.com/pkg/sftp"
)

// File is the thing being processed.
type File interface {
	io.Reader
	Name() string
}

// BackendFunc fetches files from its backend and sends them on the sink.
//
// Implementations must close the channel when they are done and return.
type BackendFunc func(ctx context.Context, sink chan<- File) error

// OnFileFunc processes file.
type OnFileFunc func(ctx context.Context, file File) error

// Thunk is a callback for various, though purely side-effect reasons.
type Thunk func(ctx context.Context) error

// Handler stores callbacks for use in Digest.
type Handler struct {
	// OnFile is invoked on any file encountered.
	OnFile OnFileFunc
	// OnSuccess is invoked once and only if no error occured.
	OnSuccess Thunk
	// OnCompletion is invoked once after all operations are complete and before OnSuccess.
	OnCompletion Thunk
}

// Digest runs the backend and invokes handler.OnFile on each file received.
//
// Of all callbacks, only OnFile is required.
func Digest(ctx context.Context, backend BackendFunc, handler *Handler) error {
	results := make(chan File)
	errs := aggregaterr.New("failed digesting: ")
	var backendErr error

	if handler.OnFile == nil {
		return fmt.Errorf("OnFile callback required")
	}

	go func() {
		err := backend(ctx, results)
		if err != nil {
			backendErr = err
		}
	}()

Loop:
	for {
		select {
		case file, more := <-results:
			if !more {
				break Loop
			}
			err := handler.OnFile(ctx, file)
			if err != nil {
				errs.Append(err)
				continue Loop
			}
		case <-ctx.Done():
			errs.Append(ctx.Err())
			break Loop
		}
	}

	// backendErr must be appended here to avoid mutex in aggregate error.
	if backendErr != nil {
		errs.Append(backendErr)
	}

	if handler.OnCompletion != nil {
		err := handler.OnCompletion(ctx)
		if err != nil {
			errs.Append(err)
		}
	}

	err := errs.Return()
	if err != nil {
		return err
	}
	if handler.OnSuccess != nil {
		return handler.OnSuccess(ctx)
	}
	return nil
}

// SFTPBackend fetches files via SFTP.
func SFTPBackend(client *sftp.Client, directory string) BackendFunc {
	return func(ctx context.Context, sink chan<- File) error {
		defer close(sink)

		fileInfos, err := client.ReadDir(directory)
		if err != nil {
			return fmt.Errorf("failed reading SFTP directory %s: %w", directory, err)
		}

		errs := aggregaterr.New("failed reading some SFTP files: ")

		for _, info := range fileInfos {
			absName := directory + "/" + info.Name()
			remoteFile, err := client.Open(absName)
			if err != nil {
				errs.Append(err)
				continue
			}
			sink <- remoteFile
		}

		return errs.Return()
	}
}
