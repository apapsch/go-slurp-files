package slurpfiles

import (
	"context"
	"io"
	"io/ioutil"
	"os"
	"path"
	"testing"
	"time"

	"github.com/pkg/sftp"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"golang.org/x/crypto/ssh"
)

func TestComponent(t *testing.T) {
	must := require.New(t)
	should := assert.New(t)
	ctx := context.Background()

	// This component test assumes a SFTP server is running on localhost:9999.
	// There are a few more hardcoded assumptions down there. Run `just` to set
	// up the expected SFTP server and run tests. https://github.com/casey/just

	sshUser := os.Getenv("USER")
	curDir, err := os.Getwd()
	must.Nil(err)
	filesDir := curDir + "/testserver/files"

	pemBytes, err := ioutil.ReadFile("testserver/id_rsa")
	must.Nil(err, "SSH key not found. Use `just` tool to run test suite.")

	signer, err := ssh.ParsePrivateKeyWithPassphrase(pemBytes, []byte("test"))
	must.Nil(err)

	sshClient, err := ssh.Dial("tcp", "localhost:9999", &ssh.ClientConfig{
		User: sshUser,
		// alright for testing
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(signer),
		},
		Timeout: 5 * time.Second,
	})
	must.Nil(err)

	sftpClient, err := sftp.NewClient(sshClient)
	must.Nil(err)

	onFile := 0
	onSuccess := 0
	onCompletion := 0
	err = Digest(
		ctx,
		SFTPBackend(sftpClient, filesDir),
		&Handler{
			OnFile: func(ctx context.Context, file File) error {
				should.Equal("hello.txt", path.Base(file.Name()))
				contents, err := io.ReadAll(file)
				should.Nil(err)
				should.Equal("hello, world\n", string(contents))
				onFile++
				return nil
			},
			OnSuccess: func(ctx context.Context) error {
				onSuccess++
				return nil
			},
			OnCompletion: func(ctx context.Context) error {
				onCompletion++
				return nil
			},
		},
	)
	must.Nil(err)
	must.Equal(1, onFile, "OnFile was not invoked")
	must.Equal(1, onSuccess, "OnSuccess was not invoked")
	must.Equal(1, onCompletion, "OnCompletion was not invoked")
}
