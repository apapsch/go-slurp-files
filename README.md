# slurp-files

Process files from SFTP backend. See the test for a complete example. To run the
test use [just](https://github.com/casey/just).
